import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Menu,
  Responsive,
  Segment,
  Visibility,
} from 'semantic-ui-react';

import NavigationItems from '../NavigationItems';

import HomepageHeading from '../HomepageHeading/HomePageHeading';

const getScreenSize = require('../../Utils/module/getScreenSize/getScreenSize');

class DesktopContainer extends Component {
    state = {}

    hideFixedMenu = () => this.setState({ fixed: false })

    showFixedMenu = () => this.setState({ fixed: true })

    render() {
      const { children } = this.props;
      const { fixed } = this.state;

      return (
        <Responsive getWidth={getScreenSize} minWidth={Responsive.onlyTablet.minWidth}>
          <Visibility
            once={false}
            onBottomPassed={this.showFixedMenu}
            onBottomPassedReverse={this.hideFixedMenu}
          >
            <Segment
              inverted
              textAlign="center"
              style={{ minHeight: 700, padding: '1em 0em' }}
              vertical
            >
              <Menu
                fixed={fixed ? 'top' : null}
                inverted={!fixed}
                pointing={!fixed}
                secondary={!fixed}
                size="large"
              >

                <NavigationItems />
              </Menu>
              <HomepageHeading />
            </Segment>
          </Visibility>
          {children}
        </Responsive>
      );
    }
}

DesktopContainer.propTypes = {
  children: PropTypes.node,
};

export default DesktopContainer;
