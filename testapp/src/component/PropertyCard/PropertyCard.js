import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, Icon, Image } from 'semantic-ui-react';

const getImage = require('../../Utils/module/getImage/getImage');

class PropertyCard extends Component {
  static propTypes = {
    propertyName: PropTypes.string,
    liftDistance: PropTypes.string,
    propertyDescription: PropTypes.string,
    paxNumber: PropTypes.string,
    propertyImage: PropTypes.number,
  };


  render() {
    const { propertyName, liftDistance, propertyDescription, paxNumber, propertyImage } = this.props;
    return (

      <Card>
        <Image src={getImage(propertyImage)} />
        <Card.Content>
          <Card.Header>{propertyName}</Card.Header>
          <Card.Meta>
            <span className="date">{liftDistance}</span>
          </Card.Meta>
          <Card.Description style={{ height: '220px', overflow: 'hidden' }}>{propertyDescription}</Card.Description>
        </Card.Content>
        <Card.Content extra>

          <Icon name="user" />
          {paxNumber}

        </Card.Content>
      </Card>

    );
  }
}


export default PropertyCard;
