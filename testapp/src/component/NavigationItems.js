import React, { Component } from 'react';
import { Menu, Container } from 'semantic-ui-react';

import { NavLink } from 'react-router-dom';


class NavigationItems extends Component {
  render() {
    return (
      <Container>
        <Menu.Item>
          <NavLink to="/">Home</NavLink>
        </Menu.Item>
        <Menu.Item><NavLink to="/filterproperties">Find a Property</NavLink></Menu.Item>
        <Menu.Item><NavLink to="/about">About</NavLink></Menu.Item>
      </Container>
    );
  }
}
export default NavigationItems;
