import PropTypes from 'prop-types';
import React from 'react';
import { NavLink } from 'react-router-dom';
import {
  Button,
  Container,
  Header,
  Icon,
} from 'semantic-ui-react';

const HomepageHeading = ({ mobile }) => (
  <Container text>
    <Header
      as="h1"
      content="Niseko Central"
      inverted
      style={{
        fontSize: mobile ? '2em' : '4em',
        fontWeight: 'normal',
        marginBottom: 0,
        marginTop: mobile ? '1.5em' : '3em',
      }}
    />
    <Header
      as="h2"
      content="Stay. Play. Experience. Find your holiday in the heart of Niseko."
      inverted
      style={{
        fontSize: mobile ? '1.5em' : '1.7em',
        fontWeight: 'normal',
        marginTop: mobile ? '0.5em' : '1.5em',
      }}
    />
    <NavLink to="login">
      <Button primary size="huge">
        Login to book
        <Icon name="right arrow" />
      </Button>
    </NavLink>
  </Container>
);
HomepageHeading.propTypes = {
  mobile: PropTypes.bool,
};

export default HomepageHeading;
