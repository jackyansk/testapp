import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Grid, Image, Input, Header, Label } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

const nclogo = require('../../resources/images/niseko-central-logo.png');

export default class HeaderBar extends PureComponent {
    static propTypes = {
      filterUpdate: PropTypes.func,
    }

    state = {
      dateTime: moment().format('MMMM Do YYYY, h:mm:ss a'),
    }

    refreshTime() {
      this.setState({
        dateTime: moment().format('MMMM Do YYYY, h:mm:ss a'),
      });
    }

    handleChange(value) {
      this.props.filterUpdate(value);
    }

    render() {
      const { dateTime } = this.state;

      window.setInterval(() => this.refreshTime(), 1000);

      return (
        <Header as="h3" block style={{ backgroundColor: 'black' }}>
          <Grid columns="equal">
            <Grid.Column>
              <NavLink to="/">
                <Image src={nclogo} size="small" />
              </NavLink>
            </Grid.Column>
            <Grid.Column width={8} style={{ alignSelf: 'center' }}>
              <Input icon="search" placeholder="Search..." style={{ width: '70%' }} onChange={event => this.handleChange(event.target.value)} />
            </Grid.Column>
            <Grid.Column style={{ alignSelf: 'center' }}>
              <Label style={{ alignSelf: 'center', width: '100%', backgroundColor: 'transparent', color: 'white' }}>
                {dateTime}
              </Label>
            </Grid.Column>
          </Grid>
        </Header>

      );
    }
}
