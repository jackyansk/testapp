import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './screens/Home';
import FilterPropertiesPage from './screens/FilterPropertiesPage';
import About from './screens/About';
import Error404 from './screens/Error404';
import Login from './screens/Login';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/filterproperties" component={FilterPropertiesPage} />
            <Route path="/about" component={About} />
            <Route path="/login" component={Login} />
            <Route component={Error404} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
