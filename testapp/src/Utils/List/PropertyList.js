//IMPORT PACKAGES
import React, {Component} from 'react';

import {Container,Grid} from 'semantic-ui-react';

//IMPORT COMPONENTS
import PropertyCard from '../../component/PropertyCard/PropertyCard';


class PropertyList extends Component {


	render() {

        const {data, filterText} =this.props;

        const propertiesList = data
        .filter(property=>{
            return property.name.toLowerCase().indexOf(filterText.toLowerCase()) >= 0 || property.description.toLowerCase().indexOf(filterText.toLowerCase()) >= 0
        })
        
        .map((property)=>{
            return(
                <Grid.Column width={4} key = {property.id}>
                    <PropertyCard
                    propertyImage ={property.id}
                    propertyName = {property.name}
                    liftDistance ={property.liftDistanceId +'m from gondola'}
                    propertyDescription ={property.description}
                    paxNumber ={property.standardPax + ' pax'}
                    />
                    </Grid.Column>            
            )
        })
		
		return (
            <Container>
                <Grid  stackable style={{alignSelf:'center'}}>
                {propertiesList}
                </Grid>
             </Container>  
		);
	}
}


export default PropertyList;