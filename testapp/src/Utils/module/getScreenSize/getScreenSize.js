const Responsive = require('semantic-ui-react');

const getScreenSize = () => {
  const isSSR = typeof window === 'undefined';
  console.log('the SSR value ' + isSSR);
  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth;
};

module.exports = getScreenSize;
