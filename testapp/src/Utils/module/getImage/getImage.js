const getImage = (imageName) => { 
  const result = require('../../../resources/images/'+imageName+'.jpg');
  return result;
};

module.exports = getImage;

/* MOK's implementation getting image url from  a json file
const json = {
  6: {
    url: '../../../../src/resources/images/6.jpg',
  },
  7: {
    url: '../../../../src/resources/images/7.jpg',
  },
};

const getImage = propertyId => json[propertyId] && json[propertyId].url;

module.exports = getImage;
*/
