import testImage1 from '../../../resources/images/6.jpg';
import testImage2 from '../../../resources/images/7.jpg';
import testImage3 from '../../../resources/images/1091.jpg';

const getImage = require('./getImage');


test('get image with index 6', () => {
  expect(getImage('6')).toBe(testImage1);
});

test('get image with index 7', () => {
  expect(getImage('7')).toBe(testImage2);
});

test('get image with index 1091', () => {
  expect(getImage('1091')).toBe(testImage3);
});
