import React, { PureComponent } from 'react';
import { Input } from 'semantic-ui-react';

class Login extends PureComponent {
  render() {
    return (
      <div>
        <div>
          <p> Username</p>
          <Input focus placeholder="Username" />
        </div>
        <div>
          <p> Password</p>
          <Input focus placeholder="Password" />
        </div>
      </div>
    );
  }
}

export default Login;
