import React, { Component } from 'react';

import PropertiesList from '../Utils/List/PropertyList';

import HeaderBar from '../component/HeaderBar/HeaderBar';

import propertiesdata from '../resources/data/properties';


class FilterPropertiesPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: '',
    };
  }

  filterUpdate(value) {
    this.setState({
      filterText: value,
    });
  }

  render() {
    return (
      <div className="App">
        <HeaderBar
          filterText={this.state.filterText}
          filterUpdate={this.filterUpdate.bind(this)}
        />


        <PropertiesList
          data={propertiesdata}
          filterText={this.state.filterText}
        />
      </div>
    );
  }
}

export default FilterPropertiesPage;
