import React from 'react';
import {
  Container,
  Grid,
  Header,
  Image,
  List,
  Segment,

} from 'semantic-ui-react';

import ResponsiveContainer from '../component/Container/ResponsiveContainer';


const mokprofpic = require('../resources/images/mok.png');
const sekkapic = require('../resources/images/Sekka.jpg');

const Home = () => (
  <ResponsiveContainer>
    <Segment style={{ padding: '8em 0em' }} vertical>
      <Grid container stackable verticalAlign="middle">
        <Grid.Row>
          <Grid.Column width={8}>
            <Header as="h3" style={{ fontSize: '2em' }}>
              Stay at the heart of Niseko
            </Header>
            <p style={{ fontSize: '1.33em' }}>
           With over 200 stays to choose from and over 15 years
          experience, we are Niseko premier accommodation provider.
           We look forward to welcoming you to Niseko.
            </p>
          </Grid.Column>
          <Grid.Column floated="right" width={6}>
            <Image bordered rounded size="large" src={sekkapic} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
        </Grid.Row>
      </Grid>
    </Segment>
    <Segment style={{ padding: '0em' }} vertical>
      <Grid celled="internally" columns="equal" stackable>
        <Grid.Row textAlign="center">
          <Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
            <Header as="h3" style={{ fontSize: '2em' }}>
            &quot;Voted Best Property in 2018&quot;
            </Header>
            <p style={{ fontSize: '1.33em' }}>Our signature suite Sekka Kan has been voted as one of the best property in Niseko!</p>
          </Grid.Column>
          <Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
            <Header as="h3" style={{ fontSize: '2em' }}>
                &quot;JAPOOOOOOOWWWW.&quot;
            </Header>
            <p style={{ fontSize: '1.33em' }}>
              <Image avatar src={mokprofpic} />
              <b>Customer</b>
              Mok Yun Liu
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>

    <Segment inverted vertical style={{ padding: '5em 0em' }}>
      <Container>
        <Grid divided inverted stackable>
          <Grid.Row>
            <Grid.Column width={3}>
              <Header inverted as="h4" content="Our Partners" />
              <List link inverted>
                <List.Item><a href="https://www.skyeniseko.com/">Skye Niseko</a></List.Item>
                <List.Item><a href="https://www.nisekocentral.com/">Niseko Central</a></List.Item>
                <List.Item><a href="https://www.kiniseko.com//">Ki Niseko</a></List.Item>
                <List.Item><a href="https://www.gosnowniseko.com/">Go Snow</a></List.Item>

              </List>
            </Grid.Column>
            <Grid.Column width={3}>
              <Header inverted as="h4" content="Services" />
              <List link inverted>
                <List.Item><a href="https://www.skyexpress.jp/">Sky Express</a></List.Item>
                <List.Item><a href="https://nisekotaxi.com/">Sprint Taxi</a></List.Item>
              </List>
            </Grid.Column>
            <Grid.Column width={7}>
              <Header as="h4" inverted>
              What are you waiting for?
              </Header>
              <p>
                 Book a property and fly to Niseko Now!
              </p>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </Segment>
  </ResponsiveContainer>
);

export default Home;
