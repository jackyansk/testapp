import React from 'react';

const Error404 = () => (
  <p>ERROR 404: PAGE NOT FOUND.</p>
);

export default Error404;
